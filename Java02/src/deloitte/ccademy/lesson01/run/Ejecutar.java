package deloitte.ccademy.lesson01.run;

import java.util.ArrayList;
import java.util.List;

import deloitte.ccademy.lesson01.array.ListaDeBooleans;
import deloitte.ccademy.lesson01.array.ListaDeCadenas;
import deloitte.ccademy.lesson01.array.ListaDeEnteros;

/**
 * Esta clase ejecuta los metodos de las clases Date y Course Permite agregar,
 * remover y ordenar datos de las listas
 * 
 * @author dareyes
 *
 */
public class Ejecutar {
	
	public static void main(String[] args) {
		
	
	//Metodos de lista de enteros
		
	System.out.print("METODOS PARA LISTA DE ENTEROS \n");
	System.out.print("La lista original es: ");
	Integer[] listaOriginal = { 3, 4, 6, 19, 19, 76};	
	
	//imprimir lista
	for (Integer num : listaOriginal) {
		System.out.print(num + ", ");
	}
	
	//Mandar llamar el metodo para imprimir el numero mas alto
	Integer mayor_numero = ListaDeEnteros.numeroMayor(listaOriginal);
	System.out.println("\nEl numero mas alto de la lista es: " + mayor_numero);
	
	//Mandar llamar el metodo para imprimir el numero mas alto
	Integer menor_numero = ListaDeEnteros.numeroMenor(listaOriginal);
	System.out.println("El numero menor de la lista es: " + menor_numero);
		
	//Mandar llamar el metodo para imprimir el numero mas alto
	List<Integer> array = ListaDeEnteros.valoresRepetidos(listaOriginal);
	System.out.println("Los valores que se repiten en la lista son: " + array);
	
	//Metodos de lista de cadena
	System.out.print("\nMETODOS PARA LISTA DE CADENAS \n");
	System.out.print("La lista original es: ");
	String[] listaCadena = { "Hola", "mi", "nombre", "es", "Daniela"};
	//imprimir lista
		for (String num : listaCadena) {
			System.out.print(num + " ");
		}
		System.out.println("");
	
	String palabraclave = ListaDeCadenas.buscarPalabra(listaCadena);
	System.out.println("La palabra que buscaste es: " + palabraclave);
	
	//Metodo para lista de Booleans
	System.out.print("\nMETODOS PARA LISTA DE BOOLEANS \n");
	System.out.print("La lista original es: ");
	Boolean[] listaBoolean = { true, false, false, true, true};
	//imprimir lista
			for (Boolean num : listaBoolean) {
				System.out.print(num + " ");
			}
			System.out.println("");
	//Mandar llamar metodo
	List<ArrayList<Boolean>> listaNueva = ListaDeBooleans.separarDatos(listaBoolean);
	System.out.print("La lista filtrada es: " + listaNueva);
	


		
	}
}


