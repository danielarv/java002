package deloitte.ccademy.lesson01.run;

import java.util.ArrayList;


import deloitte.ccademy.lesson01.array.Course;
import deloitte.ccademy.lesson01.array.Fecha;

public class BorradorEjecutarCourse {

	public static void main(String[] args) {
		
		
			ArrayList<Course> listaDeNombres = new ArrayList<Course>();

			// Crear objetos para agregarlos a la listaDeNombres
			Course registro1 = new Course();
			Course registro2 = new Course();
			Course registro3 = new Course();
			Course nuevoDato = new Course();
			Course nuevoDato2 = new Course();

			// Darle atributos a la lista
			registro1.setNombre("Daniela");
			registro2.setNombre("Andrea");
			registro3.setNombre("Anahi");

			// Agregar objetos a la lista
			listaDeNombres.add(registro1);
			listaDeNombres.add(registro2);
			listaDeNombres.add(registro3);

			// Imprimir lista original
			System.out.println("La lista original de nombres es: ");
			for (Course elemento : listaDeNombres) {
				System.out.print(elemento.getNombre() + ", ");
			}

			// Agregar datos a la lista
			nuevoDato.setNombre("Fernanda");
			nuevoDato2.setNombre("Rosa");

			listaDeNombres.add(nuevoDato);
			listaDeNombres.add(nuevoDato2);
			
			System.out.println("\n");

			// Imprimir nueva lista
			System.out.println("La lista nueva de nombres es: "); 
			for (Course elemento1 : listaDeNombres) {
				System.out.print(elemento1.getNombre() + ", ");
			}
			
			System.out.println("\n");
			
			//remover datos de una lista mandando llamar la posicion
			listaDeNombres.remove(0);
			System.out.println("La lista nueva de nombres es: "); 
			for (Course posicion : listaDeNombres) {
				System.out.print(posicion.getNombre() + ", ");
			}
			
			
			System.out.println("\n");
			//lista de fechas
			
			ArrayList<Fecha> listaDeFechas = new ArrayList<Fecha>();

			// Crear objetos para agregarlos a la listaDeNombres
			Fecha registro5 = new Fecha(1996, 4, 19);
			Fecha registro6 = new Fecha(1994, 9, 11);
			Fecha registro7 = new Fecha(1922, 10, 8);
			Fecha nuevaFecha = new Fecha(200, 8, 12);
			Fecha nuevaFecha1 = new Fecha(1994, 3, 23);


			// Agregar objetos a la lista
			listaDeFechas.add(registro5);
			listaDeFechas.add(registro6);
			listaDeFechas.add(registro7);

			// Imprimir lista original
			System.out.println("La lista original de fechas es: ");
			for (Fecha elemento : listaDeFechas) {
				System.out.print(elemento.getYear()+ "/" + elemento.getMonth() + "/" + elemento.getDate() + ", ");
			}


			listaDeFechas.add(nuevaFecha);
			listaDeFechas.add(nuevaFecha1);
			
			System.out.println("\n");

			// Imprimir nueva lista
			System.out.println("La lista nueva de fechas es: "); 
			for (Fecha elemento1 : listaDeFechas) {
				System.out.print(elemento1.getYear()+ "/" + elemento1.getMonth() + "/" + elemento1.getDate() + ", ");
			}

			System.out.println("\n");
			
			
			}

	}


