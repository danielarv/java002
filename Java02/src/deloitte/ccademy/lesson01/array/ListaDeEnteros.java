package deloitte.ccademy.lesson01.array;

import java.util.List;
import java.util.logging.Logger;



import java.util.ArrayList;


/**
 * Esta clase contiene metodos para conocer los datos mayores, menores y
 * repetidos dentro de una lista
 * 
 * @author dareyes
 *
 */

public class ListaDeEnteros {

	public static final Logger LOGGER = Logger.getLogger(ListaDeEnteros.class.getName());
	
	/**
	 * Metodo para conocer el numero mayor dentro de una lista
	 * 
	 * @param numbers es el nombre de la lista
	 * @return regresa el valor entero del numero maximo
	 */

	public static Integer numeroMayor(Integer[] numbers) {

		// La variable maximo toma el valor de la posicion 0
		Integer maximo = numbers[0];

		// El ciclo for hace la comparacion de cada numero con otro para seleccionar el
		// dato mayor
		try {
		for (Integer num : numbers) {
			if (num > maximo) {
				maximo = num;
			}
		}
		
	} catch (Exception e) {
		LOGGER.info("ERROR");
	}
		return maximo;
	}

	/**
	 * Metodo para conocer el numero menor dentro de una lista llamada numbers
	 * 
	 * @param numbers es el nombre de la lista
	 * @return regresa el valor entero del numero guardado en la variable minimo
	 */
	public static Integer numeroMenor(Integer[] numbers) {

		// La variable minimo toma el valor de la posicion 0
		Integer minimo = numbers[0];

		// El ciclo for hace la comparacion de cada numero con otro para seleccionar el
		// dato menor
		for (Integer num : numbers) {
			if (num < minimo) {
				minimo = num;
			}
		}
		return minimo;
			}

	/**
	 * Metodo para imprimir una lista con valores repetidos de lista original
	 * 
	 * @param numbers nombre de la lista
	 * @return regresa la lista
	 */
	public static List<Integer> valoresRepetidos(Integer[] numbers) {

		List<Integer> array = new ArrayList<Integer>();
		
	
			Integer i;
			
			//Ciclo para recorrer la lista y comparar datos de manera horizontal
	        for (i = 0; i < numbers.length; i++)  
	        { 
	        	for (Integer j = i+1; j<numbers.length; j++) {
	        		
	        		if (numbers[i] == numbers[j]) {
	        			array.add(numbers[i]);
		            } 
		       
	        	}
	
	        }
		return array;
	}
	

	public static void ImprimirLista (List<Integer> array) {
		for (Integer num : array) {
			System.out.print(num + ",");
	}
			
}
}

