package deloitte.ccademy.lesson01.array;

/**
 * Esta clase contiene los atributos con los que se crearan listas 
 * @author dareyes
 *
 */
public class Course {

	//Atributos
	String nombre;
	
	//Constructor
	public Course(String nombre) {
		this.nombre = nombre;
	}
	
	public Course () {
		
	}

	
	//Getters and Setters
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}




	
