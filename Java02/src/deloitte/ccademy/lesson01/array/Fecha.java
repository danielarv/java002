package deloitte.ccademy.lesson01.array;
/**
 * Esta clase contiene los atributos para crear las listas con el formato Date
 * @author dareyes
 *
 */

public class Fecha{
		
		//Atributos de la clase Date
		int year;
		int month;
		int date;
		
		//Constructor de la clase Date
		public Fecha(int year, int month, int date) {
			this.year = year;
			this.month = month;
			this.date = date;
		}
		
		public Fecha () {
			
		}

		//Getters and Setters de los Atributos
		public int getYear() {
			return year;
		}

		public void setYear(int year) {
			this.year = year;
		}

		public int getMonth() {
			return month;
		}

		public void setMonth(int month) {
			this.month = month;
		}

		public int getDate() {
			return date;
		}

		public void setDate(int date) {
			this.date = date;
		}
	

	
	}
