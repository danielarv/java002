package deloitte.ccademy.lesson01.array;

/**
 * Esta clase crea un metodo para buscar una palabra en una cadena
 * 
 * @author dareyes
 *
 */

public class ListaDeCadenas {

	public static String buscarPalabra(String[] cadena) {

		String clave = "Daniela";

		for (String elemento : cadena) {
			if (elemento == clave) {
				System.out.println("Tu palabra se encuentra dentro de la cadena.");
			}

		}
		return clave;
	}
}
