package deloitte.ccademy.lesson01.array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


public class ListaDeBooleans {
	
	private static final Logger LOGGER = Logger.getLogger(ListaDeBooleans.class.getCanonicalName());
	

	public static List<ArrayList<Boolean>> separarDatos(Boolean[] listaDeDatos) {
		
		//Lista de listas para regresar un resultado final
		List<ArrayList<Boolean>> resultado = new ArrayList<>();
		
		//Creacion de listas para filtrar resultados 
		ArrayList<Boolean> verdaderos = new ArrayList<Boolean>();
		ArrayList<Boolean> falsos = new ArrayList<Boolean>();
		
		//Ciclo para recorrer la lista y separar true de false y agregarlo a listas diferentes
		try {
		for(Boolean objeto : listaDeDatos)
		{
			if(objeto.equals(true))
			{	
				verdaderos.add(objeto);
				
			} else{
				falsos.add(objeto);
			}
			
			
		}
		} catch (Exception e) {
			LOGGER.info("ERROR");
		}
		//Imprimir las listas diferentes
		resultado.add(verdaderos);
		resultado.add(falsos);
			
		return resultado;
}
	
}